package com.isw.svcord28.api.v1;

import com.isw.svcord28.domain.svcord.service.WithdrawalDomainService;
import com.isw.svcord28.sdk.api.v1.api.WithdrawlApiV1Delegate;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import com.isw.svcord28.sdk.api.v1.model.ErrorSchema;
import com.isw.svcord28.sdk.api.v1.model.WithdrawalBodySchema;
import com.isw.svcord28.sdk.api.v1.model.WithdrawalResponseSchema;
import com.isw.svcord28.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord28.sdk.domain.svcord.entity.WithdrawalDomainServiceInput;
import com.isw.svcord28.sdk.domain.svcord.entity.WithdrawalDomainServiceOutput;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Optional;


/**
 * A stub that provides implementation for the WithdrawlApiV1Delegate
 */
@SuppressWarnings("unused")
@Service
@ComponentScan(basePackages = "com.isw.svcord28.sdk.api.v1.api")
public class WithdrawlApiV1Provider implements WithdrawlApiV1Delegate {

  @Autowired
  WithdrawalDomainService withdrawalDomainService;

  @Autowired
  DomainEntityBuilder entityBuilder;

  @Override
  public ResponseEntity<WithdrawalResponseSchema> servicingOrderWithdrawalPost(WithdrawalBodySchema withdrawalBodySchema) {
    //TODO Auto-generated method stub

    WithdrawalResponseSchema withdrawalResponseSchema = new WithdrawalResponseSchema();

    WithdrawalDomainServiceInput withdrawalDomainServiceInput = entityBuilder.getSvcord().getWithdrawalDomainServiceInput()
    .setAccountNumber(withdrawalBodySchema.getAccountNumber())
    .setAmount(withdrawalBodySchema.getAmount())
    .build();

    withdrawalDomainService.execute(withdrawalDomainServiceInput);

    return ResponseEntity.ok(null);
  }

}
