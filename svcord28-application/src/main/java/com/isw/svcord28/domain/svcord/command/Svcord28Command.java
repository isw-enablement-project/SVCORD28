package com.isw.svcord28.domain.svcord.command;


import org.springframework.stereotype.Service;
import com.isw.svcord28.sdk.domain.svcord.command.Svcord28CommandBase;
import com.isw.svcord28.sdk.domain.svcord.entity.Svcord28Entity;
import com.isw.svcord28.sdk.domain.svcord.entity.ThirdPartyReferenceEntity;
import com.isw.svcord28.sdk.domain.svcord.type.ServicingOrderType;
import com.isw.svcord28.sdk.domain.svcord.type.ServicingOrderWorkProduct;
import com.isw.svcord28.sdk.domain.svcord.type.ServicingOrderWorkResult;
import com.isw.svcord28.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord28.sdk.domain.facade.Repository;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class Svcord28Command extends Svcord28CommandBase {

  private static final Logger log = LoggerFactory.getLogger(Svcord28Command.class);

  public Svcord28Command(DomainEntityBuilder entityBuilder,  Repository repo ) {
    super(entityBuilder,  repo );
  }

  
    
    @Override
    public com.isw.svcord28.sdk.domain.svcord.entity.Svcord28 createServicingOrderProducer(com.isw.svcord28.sdk.domain.svcord.entity.CreateServicingOrderProducerInput createServicingOrderProducerInput)  {
    

      log.info("Svcord28Command.createServicingOrderProducer()");
      // TODO: Add your command implementation logic
   
      Svcord28Entity servicingOrderProcedure = this.entityBuilder.getSvcord().getSvcord28().build();
      servicingOrderProcedure.setCustomerReference(createServicingOrderProducerInput.getCustomerReference());
      servicingOrderProcedure.setProcessStartDate(LocalDate.now());
      servicingOrderProcedure.setServicingOrderType(ServicingOrderType.PAYMENT_CASH_WITHDRAWALS);
      servicingOrderProcedure.setServicingOrderWorkDescription("CASH WITHDRAWALS");
      servicingOrderProcedure.setServicingOrderWorkProduct(ServicingOrderWorkProduct.PAYMENT);
      servicingOrderProcedure.setServicingOrderWorkResult(ServicingOrderWorkResult.PROCESSING);

      ThirdPartyReferenceEntity thirdPartyReference = new ThirdPartyReferenceEntity();
      thirdPartyReference.setId("test1");
      thirdPartyReference.setPassword("password");
      servicingOrderProcedure.setThirdPartyReference(thirdPartyReference);

      return repo.getSvcord().getSvcord28().save(servicingOrderProcedure);
    }
  
    
    @Override
    public void updateServicingOrderProducer(com.isw.svcord28.sdk.domain.svcord.entity.Svcord28 instance, com.isw.svcord28.sdk.domain.svcord.entity.UpdateServicingOrderProducerInput updateServicingOrderProducerInput)  { 

      log.info("Svcord28Command.updateServicingOrderProducer()");
      // TODO: Add your command implementation logic
      
     Svcord28Entity servicingOrderProcedure = this.repo.getSvcord().getSvcord28().getReferenceById(Long.parseLong(updateServicingOrderProducerInput.getUpdateID()));
     servicingOrderProcedure.setProcessEndDate(LocalDate.now());
     servicingOrderProcedure.setServicingOrderWorkResult(updateServicingOrderProducerInput.getServicingOrderWorkResult());

      this.repo.getSvcord().getSvcord28().save(servicingOrderProcedure);

    }
  
}
