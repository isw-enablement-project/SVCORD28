package com.isw.svcord28.domain.svcord.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

import com.isw.svcord28.sdk.domain.svcord.entity.CreateServicingOrderProducerInput;
import com.isw.svcord28.sdk.domain.svcord.entity.CustomerReferenceEntity;
import com.isw.svcord28.sdk.domain.svcord.entity.Svcord28;
import com.isw.svcord28.sdk.domain.svcord.entity.UpdateServicingOrderProducerInput;
import com.isw.svcord28.sdk.domain.svcord.entity.WithdrawalDomainServiceOutput;
import com.isw.svcord28.sdk.domain.svcord.service.WithdrawalDomainServiceBase;
import com.isw.svcord28.sdk.domain.svcord.type.ServicingOrderWorkResult;
import com.isw.svcord28.sdk.integration.facade.IntegrationEntityBuilder;
import com.isw.svcord28.sdk.integration.partylife.entity.RetrieveLoginInput;
import com.isw.svcord28.sdk.integration.partylife.entity.RetrieveLoginOutput;
import com.isw.svcord28.sdk.integration.paymord.entity.PaymentOrderInput;
import com.isw.svcord28.sdk.integration.paymord.entity.PaymentOrderOutput;
import com.isw.svcord28.domain.svcord.command.Svcord28Command;
import com.isw.svcord28.integration.partylife.service.RetrieveLogin;
import com.isw.svcord28.integration.paymord.service.PaymentOrder;
import com.isw.svcord28.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord28.sdk.domain.facade.Repository;

@Service
public class WithdrawalDomainService extends WithdrawalDomainServiceBase {

  private static final Logger log = LoggerFactory.getLogger(WithdrawalDomainService.class);

  @Autowired
  Svcord28Command servicingOrderProcedureCommand;

  @Autowired
  PaymentOrder paypaymentOrder;

  @Autowired
  RetrieveLogin retrieveLogin;

  @Autowired
  IntegrationEntityBuilder integrationEntityBuilder;

  public WithdrawalDomainService(DomainEntityBuilder entityBuilder,  Repository repo) {
  
    super(entityBuilder,  repo);
    
  }
  
  @NewSpan
  @Override
  public com.isw.svcord28.sdk.domain.svcord.entity.WithdrawalDomainServiceOutput execute(com.isw.svcord28.sdk.domain.svcord.entity.WithdrawalDomainServiceInput withdrawalDomainServiceInput)  {

    log.info("WithdrawalDomainService.execute()");
    // TODO: Add your service implementation logic

   
    RetrieveLoginInput retrieveInput = integrationEntityBuilder
    .getPartylife()
    .getRetrieveLoginInput()
    .setId("test1")
    .build();

    RetrieveLoginOutput retrieveLoginOutput = retrieveLogin.execute(retrieveInput);

    if(retrieveLoginOutput.getResult() != "SUCCESS") {
      return null;
    }

    CustomerReferenceEntity customerReference = this.entityBuilder
    .getSvcord()
    .getCustomerReference()
    .build();
    customerReference.setAccountNumber(withdrawalDomainServiceInput.getAccountNumber());
    customerReference.setAmount(withdrawalDomainServiceInput.getAmount());

    CreateServicingOrderProducerInput createInput = this.entityBuilder.getSvcord()
    .getCreateServicingOrderProducerInput()
    .build();
    
    createInput.setCustomerReference(customerReference);
    Svcord28 createOutput = servicingOrderProcedureCommand.createServicingOrderProducer(createInput);


    // payment

    PaymentOrderInput paymentOrderInput = integrationEntityBuilder.getPaymord().getPaymentOrderInput().build();
    paymentOrderInput.setAccountNumber(withdrawalDomainServiceInput.getAccountNumber());
    paymentOrderInput.setAmount(withdrawalDomainServiceInput.getAmount());
    paymentOrderInput.setExternalId("SCVCORD28");
    paymentOrderInput.setExternalSerive("SCVCORD28");
    paymentOrderInput.setPaymentType("CASH_WITHDRWAL");

    PaymentOrderOutput paymentOrderOutput = paypaymentOrder.execute(paymentOrderInput);

    UpdateServicingOrderProducerInput updateServicingOrderProducerInput = this.entityBuilder.getSvcord()
    .getUpdateServicingOrderProducerInput()
    .build();

    updateServicingOrderProducerInput.setUpdateID(createOutput.getId().toString());
    updateServicingOrderProducerInput
    .setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOrderOutput.getPaymentOrderReulst()));

    servicingOrderProcedureCommand.updateServicingOrderProducer(createOutput, updateServicingOrderProducerInput);

    WithdrawalDomainServiceOutput withdrawalDomainServiceOutput = this.entityBuilder.getSvcord().getWithdrawalDomainServiceOutput()
    .setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOrderOutput.getPaymentOrderReulst()))
    .setTrasactionId(paymentOrderOutput.getTransactionId())
    .build();


    return withdrawalDomainServiceOutput;
  }

}
